from django.core.management.base import BaseCommand, CommandError

from GestorDeEscuela.models import Alumno, Curso, AlumnoDePracticas


class Command(BaseCommand):
    help = 'Load data to database'

    def handle(self, *args, **options):
        Alumno.objects.bulk_create([
            Alumno(nombre="Alumno1", apellido="Apellidos1", identificacion=123456789),
            Alumno(nombre="Alumno2", apellido="Apellidos2", identificacion=123456788),
            Alumno(nombre="Alumno3", apellido="Apellidos3", identificacion=123456787),
        ])
        Curso.objects.bulk_create([
            Curso(nombre="Matematicas"),
            Curso(nombre="Español"),
        ])
        AlumnoDePracticas.objects.create(
            nombre="Alumno4",
            apellido="Apellidos4",
            identificacion=123456780,
            empresa="Solvo"
        )
