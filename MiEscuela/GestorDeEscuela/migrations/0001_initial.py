# Generated by Django 3.2.11 on 2022-01-31 16:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=32)),
                ('apellido', models.CharField(max_length=32)),
                ('identificacion', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Curso',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='AlumnoDePracticas',
            fields=[
                ('alumno_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='GestorDeEscuela.alumno')),
                ('empresa', models.CharField(max_length=64)),
            ],
            bases=('GestorDeEscuela.alumno',),
        ),
        migrations.CreateModel(
            name='ListaNota',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nota', models.PositiveSmallIntegerField()),
                ('curso', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='GestorDeEscuela.curso')),
                ('listaNotas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notas', to='GestorDeEscuela.alumno')),
            ],
        ),
    ]
