from datetime import datetime

from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from GestorDeEscuela.forms import NotasForm
from GestorDeEscuela.models import Curso, Alumno, ListaNota


@require_http_methods(["GET", "POST"])
def notas_view(request):
    if request.method == "POST":
        curso_id = request.POST.get('curso')
        errors = []
        for alumno_id, nota in request.POST.items():
            if not (alumno_id == 'curso' or alumno_id == 'csrfmiddlewaretoken'):
                form = NotasForm({
                    'curso': curso_id,
                    'alumno': alumno_id,
                    'nota': nota,
                })
                if form.is_valid():
                    form.save()
                    curso = Curso.objects.get(id=curso_id)
                    curso.posee_nota = True
                    curso.save()
                else:
                    errors += form.errors

        if not errors:
            return render(request, "notas.html", {
                'cursos': Curso.objects.filter(posee_nota=False),
                'alumnos': Alumno.objects.all(),
                'success': True,
            })
        else:
            return render(request, "notas.html", {
                'cursos': Curso.objects.filter(posee_nota=False),
                'alumnos': Alumno.objects.all(),
                'errors': errors,
            })
    return render(request, "notas.html", {
        'cursos': Curso.objects.filter(posee_nota=False),
        'alumnos': Alumno.objects.all()
    })

@csrf_exempt
def update_grade(request, alumno_id, curso_id):
    print(alumno_id, curso_id, request.POST)
    lista_nota = ListaNota.objects.get(curso_id=curso_id,alumno_id=alumno_id)
    old_grade = lista_nota.nota
    form = NotasForm(request.POST, instance=lista_nota)
    if form.is_valid():
        form.save()

    return JsonResponse({
        'nota': old_grade
    }, safe=False)

def is_prime(num):
    if num > 1:
        for i in range(2, num):
            if (num % i) == 0:
                return False
        else:
            return True
    else:
        return False

def minute_prime(request):
    now = datetime.now()
    data = {
        'chosen_number': "%s" % (now.minute),
        'is_prime': is_prime(now.minute),
        'time': now
    }
    return JsonResponse(data, safe=False)