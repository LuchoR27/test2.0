from django.apps import AppConfig


class GestordeescuelaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'GestorDeEscuela'
