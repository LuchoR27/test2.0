from django import forms

from GestorDeEscuela.models import ListaNota


class NotasForm(forms.ModelForm):
    curso = forms.CharField(required=False)
    alumno = forms.CharField(required=False)

    class Meta:
        model = ListaNota
        fields = '__all__'