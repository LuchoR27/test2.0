from django.contrib import admin

# Register your models here.
from GestorDeEscuela.models import Curso, Alumno, ListaNota, AlumnoDePracticas

class NotasAdmin(admin.TabularInline):
    model = ListaNota

class AlumnoAdmin(admin.ModelAdmin):
   inlines = [NotasAdmin,]

admin.site.register(Curso)
admin.site.register(Alumno, AlumnoAdmin)
admin.site.register(ListaNota)
admin.site.register(AlumnoDePracticas)