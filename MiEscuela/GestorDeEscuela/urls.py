from django.urls import path, include

from GestorDeEscuela.views import notas_view, minute_prime, update_grade

urlpatterns = [
    path('notas', notas_view, name="notas"),
    path('notas/<int:alumno_id>/<int:curso_id>/actualizar', update_grade, name="update_grade"),
    path('minutoprimo', minute_prime, name="minute_prime"),

]
