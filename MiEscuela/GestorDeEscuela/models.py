from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

class Curso(models.Model):
    nombre = models.CharField(max_length=64)
    posee_nota = models.BooleanField(default=False)

class Alumno(models.Model):
    nombre = models.CharField(max_length=32)
    apellido = models.CharField(max_length=32)
    identificacion = models.IntegerField()

class ListaNota(models.Model):
    nota = models.PositiveSmallIntegerField(validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
        ])
    curso = models.ForeignKey(to=Curso, on_delete=models.CASCADE)
    alumno = models.ForeignKey(to=Alumno, on_delete=models.CASCADE, related_name="notas")

class AlumnoDePracticas(Alumno):
    empresa = models.CharField(max_length=64)
