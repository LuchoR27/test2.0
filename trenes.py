
if __name__ == '__main__':
    trainSpeed1 = int(input("Digite la velocidad del tren 1 en km/h."))
    trainDistance1 = 0
    trainSpeed2 = int(input("Digite la velocidad del tren 2 en km/h."))
    trackDistance = int(input("Digite la velocidad del tren 2 en km/h."))
    trainDistance2 = trackDistance
    minutes = 0

    while trainDistance1 < trainDistance2:
        minutes += 1
        trainDistance1 += trainSpeed1
        trainDistance2 -= trainSpeed2

        if trainDistance1 == trainDistance2:
            print(f"Los trenes chocan en el km {trainDistance1} a los {minutes} minutos.")
        if trainDistance1 > trainDistance2:
            if trainSpeed1 == trainSpeed2:
                print(f"Los trenes chocan en el km {int(trackDistance/2)} a los {minutes} minutos.")
            else:
                print(f"Los trenes chocan entre el km {trainDistance1-trainSpeed1} y {trainDistance2+trainSpeed2} a los {minutes} minutos.")
